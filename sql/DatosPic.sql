INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Agencia de Modelos','Agència de Models','Models Agency', 'agencia_de_modelos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Agencia de Viajes','Agència de Viatges','Travel Agency','agencias_de_viajes.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Alferería','Alfereria','Pottery Shop', 'alfereria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Alquiller de Coches','Lloguer de Cotxes','Car Rental', 'tienda_de_alquiler_de_coches.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Ambulatorio','Ambulatori','Ambulatory', 'ambulatorio.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Apicultura','Apicultura','Bee Products', 'tienda_de_apicultura.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Arreglos de Ropa','Arranjaments de Roba','Clothing Fix', 'tienda_de_arreglos_de_ropa.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Asociación de Personas con Autismo','Associació de Persones amb Autisme','Association of people with autism', 'asociacion_de_personas_con_autimo.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Atención al público','Atenció al públic','Information', 'atencion_al_publico.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Autoescuela','Autoescola','Driving School', 'autoescuela.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Ayudas Técnicas','Ajudes Tècniques','Technical Support', 'tienda_de_ayudas_tecnicas.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Ayuntamiento','Ajuntament','Town Hall', 'ayuntamiento.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Banco','Banc','Bank', 'banco.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Bar de tapas','Bar de tapes','Tapas', 'bar_de_tapas.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Bar','Bar','Pub', 'bar.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Bebidas','Begudes','Beverage Shop', 'tienda_de_bebidas.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Biblioteca','Biblioteca','Library', 'biblioteca(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Bicicletas','Bicicletes','Bike Shop', 'tienda_de_bicicletas.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Bolsos','Bosses de mà','Handbags', 'tienda_de_bolsos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Bricolaje','Bricolatge','DIY Store', 'tienda_de_bricolaje.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Cafetería','Cafeteria','Cafe', 'cafeteria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Cajero Automático','Caixer Automàtic','ATM', 'cajero_automatico.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Campanario','Campanari','Bell Tower', 'campanario.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Capitolio','Capitoli','Capitol', 'capitolio.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Caramelos','Caramels','Candy Shop', 'tienda_de_caramelos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Carnicería','Carnisseria','Butcher', 'carniceria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Carpintería','Fusteria','Carpentry', 'carpinteria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Cartucho de tinta','Cartutx de tinta','Ink Cartridge Store', 'tienda_de_cartuchos_de_tinta.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Casa','Casa','House', 'casas.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Casino','Casino','Casino', 'casino.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Casquería','Triperia','Farmhouse', 'casqueria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Castillo','Castell','Castle', 'castillo(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Catedral','Catedral','Cathedral', 'catedral.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Caza y Pesca','Caça i Pesca','Hunting and fishing', 'tienda_de_caza_y_pesca.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Cementerio','Cementeri','Cementery', 'cementerio.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Centro Comercial','Centre Comercial','Shopping Mall', 'centro_comercial.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Cervecería','Cerveseria','Brewery', 'cerveceria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Charcutería','Xarcuteria','Delicatessen', 'charcuteria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Chiringuito','Xiringuito','Chiringuito', 'chiringuito.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Chocolatería','Xocolateria','Chocolate Shop', 'chocolateria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Churrería','Xurreria','Churreria', 'churreria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Cine','Cinema','Cinema', 'cine.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Coches','Cotxes','Car Selling', 'tienda_de_coches.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Colchones','Matalassos','Mattresses Shop', 'tienda_de_colchones.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Colegio','Col·legi','School', 'colegio(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Comercio','Comerç','Shop', 'comercio(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Comisaría','Comissaria','Police Station', 'comisaria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Complementos','Complements','Accessories', 'tienda_de_complementos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Concesionario','Concessionari','Concessionaire', 'concesionario.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Congelados','Congelats','Frozen Food', 'tienda_de_congelados.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Conservatorio','Conservatori','Conservatory', 'conservatorio.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Copistería','Copisteria','Copy Shop', 'copisteria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Correos','Correos','Post Office', 'correos(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Crepería','Creperia','Creperia', 'creperia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Cristalería','Cristalleria','Glassware', 'cristaleria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Decoración','Decoració','Decoration', 'tienda_de_decoracion.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Dentista','Dentista','Dentist', 'dentista.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Desportes','Esports','Sports', 'tienda_de_desportes.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Disfraces','Disfresses','Costume Store', 'tienda_de_disfarces.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Edificio','Edifici','Building', 'edificio.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Electricidad','Electricitat','Electrical Store', 'tienda_de_electricidad.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Electrónica','Electrònica','Electronic equipment', 'tienda_de_electronica.png.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Escayola','Escaiola','Plaster Store', 'tienda_de_escayola.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Escuela de Idiomas','Escoa d´Idiomes','Languages School', 'escuela_oficial_de_idiomas.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Escuela de Música','Escola de música','Music School', 'escuela_de_música.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Escuela de arte','Escola d´Art','Art School', 'escuela_de_arte.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Escuela','Escola','School', 'escuela.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Esquís','Esquís','Ski Equipments', 'tienda_de_esquis.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Estación','Estació','Train station', 'estacion(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Estanco','Estanc','Smoke Shop', 'estanco.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Estética','Estètica','Aesthetic shop', 'tienda_de_estetica.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Farmacia','Farmàcia','Pharmacy', 'farmacia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Feria','Fira','Fair', 'feria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Ferretería','Ferreteria','Train station', 'ferreteria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Floristería','Floristeria','Flower Shop', 'floristeria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Fontanería','Fontaneria','Plumbing', 'fontaneria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Fotografía','Fotografia','Photography', 'tienda_de_fotografia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Frutería','Fruiteria','Fruteria', 'fruteria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Funicular','Funicular','Cable Car', 'funicular.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Galletería','Galeteria','Cookies Shop', 'galleteria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Gasolinera','Benzinera','Gas Station', 'gasolinera(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Gimnasio','Gimnàs','Gym', 'gimnasio.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Hamburguesería','Hamburgueseria','Burguers', 'hamburgueseria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Heladería','Gelateria','Ice Cream', 'heladeria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Herboristería','Herboristeria','Herbalist shop', 'herboristeria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Hospital Infantil','Hospital Infantil','Children Hospital', 'hospital_infantil.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Hospital','Hospital','Hospital', 'hospital.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Hotel accesible','Hotel accessible','Accessible hotel', 'hotel_accesible.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Hotel','Hotel','Hotel', 'hotel.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Iglesia','Esglèsia','Church', 'iglesia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Industria','Indústria','Industry', 'industria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Informática','Informàtica','Informatics', 'tienda_de_informatica.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Inmobiliaria','Immobiliària','Real estate', 'inmobiliaria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Instituto','Institut','Institute', 'instituto.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Invernadero','Hivernacle','Greenhouse', 'invernadero.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Joyería','Joieria','Jewelry', 'joyeria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Juguetería','Botiga de Joguines','Toy Store', 'tienda_de_juguetes.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Kebab','Kebab','Kebab', 'kebab.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Lavandería','Bugaderia','Laundry', 'lavanderia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Lencería','Llenceria','Lingerie', 'lenceria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Librería','Llibreria','Book Shop', 'libreria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Lotería','Loteria','Lottery', 'loterias_y_apuestas.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Manualidades','Manualitats','Craft Store', 'tienda_de_manualidades.png'); 
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Material de Construcciónn','Material de Construcció','Construction Material', 'tienda_de_materiales_de_construccion.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Menaje','Parament de casa','Household Store', 'tienda_de_menaje.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Mercería','Merceria','Haberdashery', 'merceria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Metro','Metro','Metro', 'metro.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Mezquita','Mesquita','Mosque', 'mezquita.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Moda','Moda','Fashion Store', 'tienda_de_moda.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Motos','Motos','Motorbike Shop', 'tienda_de_motos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Muebles de Cocina','Mobles de Cuina','Kitchen Furniture', 'tienda_de_muebles_de_cocina.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Muebles','Mobles','Furniture Store', 'tienda_de_muebles.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Museo','Museu','Museum', 'museo.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Máquinas de Coser','Màquines de Cosir','Sweing Machine Store', 'tienda_de_maquinas_de_coser.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Música','Música','Music Store', 'tienda_de_musica.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Observatorio','Observatori','Observatory', 'observatorio.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Oficina de Empleo','Oficina de Treball','Employment Office', 'oficina_de_empleo.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Oficina de Turismo','Oficina de Turisme','Tourist Office', 'oficina_de_turismo.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Oficina','Oficina','Oficce', 'oficina.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Óptica','Òptica','Optics', 'optica.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Ortopedia','Ortopèdia','Orthopedics', 'ortopedia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Panadería','Fleca','Bakery', 'panaderia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Papelería','Papereria','Stationery', 'papelaria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Parada de Taxis','Parada de Taxis','Taxi', 'parada_de_taxis.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Parque Acuático','Parc Aquàtic','Aquatic Park', 'parque_acuatico.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Parque de Bomberos','Parc de Bombers','Fire Station', 'parque_de_bomberos(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Pastelería','Pastisseria','Cake Shop', 'pasteleria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Patinaje','Patinatge','Skating Shop', 'tienda_de_patinaje.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Peluquería Canina','Perruqueria Canina','Pet Shop', 'peluqueria_canina.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Peluquería','Perruqueria','Hairdressing', 'peluqueria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Pensión','Pensió','Pension', 'pension.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Perfumería','Perfumeria','Perfumery', 'perfumeria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Pescadería','Peixateria','Fish Shop', 'pescaderia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Pinturas','Pintures','Panting Store', 'tienda_de_pinturas.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Piscina Cuberta','Piscina Coberta','Indoor swimming pool', 'piscina_cubierta.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Pizzería','Pizzeria','Pizza', 'pizzeria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Policía Local','Policia Local','Local Police', 'policia_local.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Pollería','Pollastreria','Chicken Shop', 'polleria(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Pozo','Pou','Water Well', 'pozo.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Prisión','Presó','Prison', 'prisiones.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Productos Ecológicos','Productes Ecològics','Eco Friendly Products','tienda_de_productos_ecologicos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Puerto','Port','Port', 'puerto.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Párquing','Pàrquing','Parking', 'parking.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Quesería','Formatgeria','Cheese Shop', 'queseria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Quiosco','Quiosc','Kiosk', 'quiosco(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Regalos','Regalos','Gift Shop', 'tienda_de_regalos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Residencia de Ancianos','Residència d´Avis','Nursing Home', 'residencia_de_ancianos(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Residencia para discapacitados','Residància per a discapacitats','Disabled Nursing Home', 'residencia_para_discapacitados.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Restaurante','Restaurant','Restaurant', 'restaurante.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Ropa Infantil','Roba Infantil','Children Clothes', 'tienda_de_ropa_infantil.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Rótulos','Rètuls','Label Shop', 'tienda_de_rotulos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Sagrada Familia','Sagrada Família','Sagrada Familia', 'Sagrada_Familia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Serigrafía','Serigrafia','Serigraphy Shop', 'tienda_de_serigrafia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Sinagoga','Sinagoga','Synagogue', 'sinagoga.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Supermercat','Supermercat','Supermarket', 'supermercado.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Taller Mecánico','Taller Mecànic','Mechanical Workshop', 'taller_mecanico.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Teatro','Teatre','Theater', 'teatro(1).png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Telefonía','Telefonia','Mobile Shop', 'tienda_de_telefonia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Templo','Temple','Temple', 'templo.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Tienda de Cuadros','Botiga de Quadres','Art Painting Material','tienda_de_cuadros.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Tienda de Mascotas','Botiga de Mascotes','Pet shop', 'tienda_de_animales.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Tienda de ropa','Botiga de Roba','Clothes Shop', 'tienda_de_ropa.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Tienda de zapatos','Botiga de Sabates','Shoe shop', 'zapateria.png'); 
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Tranvía','Tramvia','Tram', 'tranvia.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Tren Turístico','Tren Turístic','Touristic Train', 'tren_turistico.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Tribunal','Tribunal','Court', 'tribunal.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Universidad','Universitat','University', 'universidad.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Verdulería','Botiga de Verdures','Greengrocery', 'verduleria.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Vinatería','Vinateria','Wine Store', 'tienda_de_vinos.png');
INSERT INTO tea (tipo_es,tipo_ca,tipo_en, pictograma) VALUES ('Zoológico','Zoològic','Zoo', 'zoologico.png');


