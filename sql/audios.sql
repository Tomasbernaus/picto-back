insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('AgenciaModelos.mp3', 'AgenciaModels.mp3', 'ModelAgency.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('AgenciaViajes.mp3', 'AgenciaViatges.mp3', 'TravelAgency.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Alfereria.mp3', 'Alfereria.mp3', 'PotteryShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('AlquilerCoches.mp3', 'LloguerCotxes.mp3', 'CarRental.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Ambulatorio.mp3', 'Ambulatori.mp3', 'AccidentEmergency.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Apicultura.mp3', 'Apicultura.mp3', 'BeeProducts.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('ArreglosRopa.mp3', 'ArranjamentsRoba.mp3', 'Tailor.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('AsociacionPersonasAutismo.mp3', 'AssociacioPersonesAutisme.mp3', 'AssociationAutism.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('AtencionPublico.mp3', 'AtencioPublic.mp3', 'Information.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Autoescuela.mp3', 'Autoescola.mp3', 'DrivingSchool.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('AyudasTecnicas.mp3', 'AjudesTecniques.mp3', 'TechnicalSupport.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Ayuntamiento.mp3', 'Ajuntament.mp3', 'TownHall.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Banco.mp3', 'Banc.mp3', 'Bank.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('BarTapas.mp3', 'BarTapes.mp3', 'TapasRestaurant.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Bar.mp3', 'Bar.mp3', 'Pub.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Bebidas.mp3', 'Begudes.mp3', 'BeverageShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Biblioteca.mp3', 'Biblioteca.mp3', 'Library.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Bicicletas.mp3', 'Bicicletes.mp3', 'BikeShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Bolsos.mp3', 'BosssesMa.mp3', 'Handbags.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Bricolaje.mp3', 'Bricolatge.mp3', 'DIYStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Cafeteria.mp3', 'Cafeteria.mp3', 'Cafe.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('CajeroAutomatico.mp3', 'CaixerAutomatic.mp3', 'ATM.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Campanario.mp3', 'Campanari.mp3', 'BellTower.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Capitolio.mp3', 'Capitoli.mp3', 'Capitol.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Caramelos.mp3', 'Caramels.mp3', 'CandyShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Carniceria.mp3', 'Carnisseria.mp3', 'Butcher.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Carpinteria.mp3', 'Fusteria.mp3', 'Carpentry.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('CartuchoTinta.mp3', 'CartutxTinta.mp3', 'InkCartridge.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Casa.mp3', 'Casa.mp3', 'House.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Casino.mp3', 'Casino.mp3', 'Casino.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Casqueria.mp3', 'Triperia.mp3', 'Farmhouse.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Castillo.mp3', 'Castell.mp3', 'Castle.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Catedral.mp3', 'Catedral.mp3', 'Cathedral.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('CazaPesca.mp3', 'CacaPesca.mp3', 'HuntingFishing.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Cementerio.mp3', 'Cementeri.mp3', 'Cemetery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('CentroComercial.mp3', 'CentreComercial.mp3', 'ShoppingMall.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Cerveceria.mp3', 'Cerveceria.mp3', 'Brewery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Charcuteria.mp3', 'Xarcuteria.mp3', 'Delicatessen.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Chiringuito.mp3', 'Xiringuito.mp3', 'Chiringuito.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Chocolateria.mp3', 'Xocolateria.mp3', 'ChocolateShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Churreria.mp3', 'Xurreria.mp3', 'Churros.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Cine.mp3', 'Cinema.mp3', 'Cinema.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Colchones.mp3', 'Matalassos.mp3', 'MattressesShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Colegio.mp3', 'Collegi.mp3', 'School.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Comercio.mp3', 'Comerc.mp3', 'Store.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Comisar¡a.mp3', 'Comissaria.mp3', 'PoliceStation.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Complementos.mp3', 'Complements.mp3', 'Accessories.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Concesionario.mp3', 'Concessionari.mp3', 'CarDeal.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Congelados.mp3', 'Congelats.mp3', 'FrozenFoodShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Conservatorio.mp3', 'Conservatori.mp3', 'Conservatory.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Copisteria.mp3', 'Copisteria.mp3', 'CopyShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Correos.mp3', 'Correos.mp3', 'PostOffice.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Creperia.mp3', 'Creperia.mp3', 'Crepes.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Cristaleria.mp3', 'Cristalleria.mp3', 'Glassware.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Decoracion.mp3', 'Decoracio.mp3', 'Decoration.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Dentista.mp3', 'Dentista.mp3', 'Dentist.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Desportes.mp3', 'Esports.mp3', 'SportsStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Disfraces.mp3', 'Disfresses.mp3', 'CostumeStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Edificio.mp3', 'Edifici.mp3', 'Building.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Electricidad.mp3', 'Electricitat.mp3', 'ElectricalStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Electronica.mp3', 'Electronica.mp3', 'ElectronicEquipment.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Escayola.mp3', 'Escaiola.mp3', 'PlasterStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('EscuelaIdiomas.mp3', 'EscolaIdiomes.mp3', 'LanguagesSchool.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('EscuelaMusica.mp3', 'EscolaMusica.mp3', 'MusicSchool.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('EscuelaArte.mp3', 'EscolaArt.mp3', 'ArtSchool.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Escuela.mp3', 'Escola.mp3', 'School.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Esquis.mp3', 'Esquis.mp3', 'SkiEquipments.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Estacion.mp3', 'Estacio.mp3', 'TrainStation.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Estanco.mp3', 'Estanc.mp3', 'TobaccoShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Estética.mp3', 'Estetica.mp3', 'AestheticShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Farmacia.mp3', 'Farmacia.mp3', 'Pharmacy.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Feria.mp3', 'Fira.mp3', 'Fair.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Ferreteria.mp3', 'Ferreteria.mp3', 'HardwareStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Floristeria.mp3', 'Floristeria.mp3', 'FlowerShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Fontaneria.mp3', 'Fontaneria.mp3', 'Plumbing.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Fotografia.mp3', 'Fotografia.mp3', 'Photography.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Fruteria.mp3', 'Fruiteria.mp3', 'Fruitgrocery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Funicular.mp3', 'Funicular.mp3', 'CableCar.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Galleteria.mp3', 'Galeteria.mp3', 'CookieShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Gasolinera.mp3', 'Benzinera.mp3', 'PetrolStation.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Gimnasio.mp3', 'Gimnas.mp3', 'Gym.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Hamburgueseria.mp3', 'Hamburgueseria.mp3', 'BurgerRestaurant.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Heladeria.mp3', 'Gelateria.mp3', 'IceCreamShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Herboristeria.mp3', 'Herboristeria.mp3', 'HerbalistShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('HospitalInfantil.mp3', 'HospitalInfantil.mp3', 'ChildrenHospital.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Hospital.mp3', 'Hospital.mp3', 'Hospital.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('HotelAccesible.mp3', 'HotelAccessible.mp3', 'AccessibleHotel.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Hotel.mp3', 'Hotel.mp3', 'Hotel.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Iglesia.mp3', 'Esglesia.mp3', 'Church.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Industria.mp3', 'Industria.mp3', 'Industry.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Informatica.mp3', 'Informatica.mp3', 'Informatics.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Inmobiliaria.mp3', 'Immobiliaria.mp3', 'RealEstateAgent.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Instituto.mp3', 'Institut.mp3', 'Institute.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Invernadero.mp3', 'Hivernacle.mp3', 'Greenhouse.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Joyeria.mp3', 'Joieria.mp3', 'Jewelry.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Jugueteria.mp3', 'BotigaJoguines.mp3', 'ToyStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Kebab.mp3', 'Kebab.mp3', 'Kebab.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Lavanderia.mp3', 'Bugaderia.mp3', 'Launderette.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Lenceria.mp3', 'Llenceria.mp3', 'LingerieShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Libreria.mp3', 'Llibreria.mp3', 'BookShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Loteria.mp3', 'Loteria.mp3', 'Lottery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Manualidades.mp3', 'Manualitats.mp3', 'CraftStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('MaterialConstruccion.mp3', 'MaterialConstruccio.mp3', 'ConstructionMaterial.mp3'); 
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Menaje.mp3', 'ParamentCasa.mp3', 'HouseholdStore.mp3'); 
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Merceria.mp3', 'Merceria.mp3', 'Haberdashery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Metro.mp3', 'Metro.mp3', 'Metro.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Mezquita.mp3', 'Mesquita.mp3', 'Mosque.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Moda.mp3', 'Moda.mp3', 'FashionStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Motos.mp3', 'Motos.mp3', 'MotorcycleDeal.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('MueblesCocina.mp3', 'MoblesCuina.mp3', 'KitchenFurniture.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Muebles.mp3', 'Mobles.mp3', 'FurnitureStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Museo.mp3', 'Musica.mp3', 'MusicStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('MaquinasCoser.mp3', 'MaquinesCosir.mp3', 'SewingMachines.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Musica.mp3', 'Muuseu.mp3', 'Museum.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Observatorio.mp3', 'Observatori.mp3', 'Observatory.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('OficinaEmpleo.mp3', 'OficinaTreball.mp3', 'EmploymentOffice.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('OficinaTurismo.mp3', 'OficinaTurisme.mp3', 'TouristOffice.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Oficina.mp3', 'Oficina.mp3', 'Office.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Optica.mp3', 'Optica.mp3', 'Optics.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Ortopedia.mp3', 'Ortopedia.mp3', 'Orthopedics.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Panaderia.mp3', 'Fleca.mp3', 'Bakery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Papeleria.mp3', 'Papereria.mp3', 'Stationery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('ParadaTaxis.mp3', 'ParadaTaxis.mp3', 'Taxi.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('ParqueAcuatic.mp3', 'ParcAquati.mp3', 'AquaticPark.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('ParqueBombero.mp3', 'ParcBomber.mp3', 'FireStation.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Pasteleria.mp3', 'Pastisseria.mp3', 'CakeShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Patinaje.mp3', 'Patinatge.mp3', 'SkatingShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('PeluqueriaCanina.mp3', 'PerruqueriaCanina.mp3', 'PetGrooming.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Peluqueria.mp3', 'Perruqueria.mp3', 'Hairdressing.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Pension.mp3', 'Pensio.mp3', 'Pension.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Perfumeria.mp3', 'Perfumeria.mp3', 'Perfumery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Pescaderia.mp3', 'Peixateria.mp3', 'Fishmonger.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Pinturas.mp3', 'Pintures.mp3', 'PantingStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('PiscinaCuberta.mp3', 'PiscinaCoberta.mp3', 'IndoorSwimming.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Pizzeria.mp3', 'Pizzeria.mp3', 'Pizza.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('PoliciaLocal.mp3', 'PoliciaLocal.mp3', 'LocalPolice.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Polleria.mp3', 'Pollastreria.mp3', 'ChickenButcher.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Pozo.mp3', 'Pou.mp3', 'WaterWell.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Prision.mp3', 'Preso.mp3', 'Prison.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('ProductosEcologicos.mp3', 'ProductesEcologics.mp3', 'EcoFriendlyProducts.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Puerto.mp3', 'Port.mp3', 'Port.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Parquing.mp3', 'Parquing.mp3', 'Parking.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Queseria.mp3', 'Formatgeria.mp3', 'CheeseShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Quiosco.mp3', 'Kiossc.mp3', 'Kiosk.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Regalos.mp3', 'Regals.mp3', 'GiftShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('ResidenciaAncianos.mp3', 'ResidenciaAvis.mp3', 'NursingHome.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('ResidenciaDiscapacitados.mp3', 'ResidenciaDiscapacitats.mp3', 'DisabledNursing.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Restaurante.mp3', 'Restaurant.mp3', 'Restaurant.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('RopaInfantil.mp3', 'RobaInfantil.mp3', 'ChildrenClothes.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Rotulos.mp3', 'Retuls.mp3', 'LabelShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('SagradaFamilia.mp3', 'SagradaFamilia.mp3', 'SagradaFamilia.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Serigrafia.mp3', 'Serigrafia.mp3', 'SerigraphyShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Sinagoga.mp3', 'Sinagoga.mp3', 'Synagogue.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Supermercat.mp3', 'Supermercat.mp3', 'Supermarket.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('TallerMecanico.mp3', 'TallerMecanic.mp3', 'MechanicalWorkshop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Teatro.mp3', 'Teatre.mp3', 'Theater.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Telefonia.mp3', 'Telefonia.mp3', 'MobileShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Templo.mp3', 'Temple.mp3', 'Temple.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('TiendaCuadros.mp3', 'BotigaQuadres.mp3', 'ArtSupplies.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('TiendaMascotas.mp3', 'BotigaMascotes.mp3', 'PetShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('TiendaRopa.mp3', 'BotigaRoba.mp3', 'ClothesShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('TiendaZapatos.mp3', 'BotigaSabates.mp3', 'ShoeShop.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Tranvia.mp3', 'Tramvia.mp3', 'Tram.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('TrenTuristico.mp3', 'TrenTuristic.mp3', 'TouristicTrain.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Tribunal.mp3', 'Tribunal.mp3', 'Court.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Universidad.mp3', 'Universitat.mp3', 'University.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Verduleria.mp3', 'BotigaVerdures.mp3', 'Greengrocery.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Vinateria.mp3', 'Vinateria.mp3', 'WineStore.mp3');
insert into tea_audios (audio_ES,audio_CA,audio_EN) VALUES ('Zoologico.mp3', 'Zoologic.mp3', 'Zoo.mp3');