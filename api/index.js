const express = require('express');
const app = express();

const model = require('./models/index');

app.use(express.static('public'));

app.get('/pictograma', function (req, res, next) {
    model.Pictograma.findAll()
        .then(tea => res.json(tea))
        .catch(error => res.json(error))
});


app.get('/pictograma/:id', function (req, res, next) {
    model.Pictograma.findOne({ where: { id: req.params.id } })
        .then(picto => res.json(picto))
        .catch(error => res.json(error))
});



app.listen(5000, () => console.log(`App en puerto 3000!`))
