import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';
import Formulario from "./Form";
import { TEXTOS } from './textos';
import Mapa from "./Mapa";
import ContactUs from "./Contact";

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      idioma: 0
    }
  }


  render() {
    return (
      <BrowserRouter>
        <Container>

          <Row>
            <Col>
              <button onClick={() => this.setState({ idioma: 0 })} >CA</button>
              <button onClick={() => this.setState({ idioma: 1 })} >ES</button>
              <button onClick={() => this.setState({ idioma: 2 })} >EN</button>
              <ul>

                <li> <Link to="/Form">{TEXTOS.sform[this.state.idioma]}</Link> </li>
                <li> <Link to="/Mapa">{TEXTOS.smap[this.state.idioma]}</Link> </li>
                <li> <Link to="/Contact">{TEXTOS.scontact[this.state.idioma]}</Link> </li>
              </ul>
            </Col>
          </Row>

          <Switch>

            <Route path="/Form" render={() => <Formulario idioma={this.state.idioma} />} />
            <Route path="/Mapa" component={Mapa} />
            <Route path="/Contact" render={() => <ContactUs idioma={this.state.idioma} />}/>
           
          </Switch>

        </Container>
      </BrowserRouter>
    );
  }
}

export default App;

//<li> <Link to="/">Home</Link> </li>
 //<li> <Link to="/pagina3">Pagina3</Link> </li>
 //<li> <Link to="/pagina4">Pagina4</Link> </li>

 //<Route exact path="/" component={Home} />
 //<Route path="/pagina3" component={Pagina3} />
 //<Route path="/pagina4" component={Pagina4} />
 //<Route component={NotFound} />

//<li> <Link to="/Contact">{TEXTOS.scontact[this.state.idioma]}</Link> </li>
//<Route path="/Contact" render={() => <Contato idioma={this.state.idioma} />} />
//import Contato from "./Contact";