import React from 'react';
import { Input } from 'reactstrap';
import { API_URL } from "./database.js";
import { TEXTOS } from './textos';

//alert("Este e-mail ya está registrado no nuestro sistema")
//alert("Aquest e-mail ja està registrat no el nostre sistema")
//alert("This e-mail is already registered on our system")

class Email extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultValue: "",
            message: "",
            emailExists: false
        };
        this.handleBlur = this.handleBlur.bind(this);
        this.checkEmail = this.checkEmail.bind(this);
    }

    componentDidMount() {

        fetch(API_URL + "tea_empresas", { method: "GET" })
            .then(data => data.json())
            .then(dadesConvertides => this.setState({ tea: dadesConvertides }))
            .catch(err => console.log(err));
    }

    handleBlur(event) {
        this.setState({ emailToCheck: event.target.value }, this.checkEmail);
    }


    checkEmail() {
        let email = this.props.email;
        console.log("email check ", email)
        // TODO validate email (you don't want to call the API if it's not a validate email)
        fetch(API_URL + "tea_empresas", { method: "GET" })
            .then(data => data.json())
            .then(empresas => {
                console.log("Empresas:", empresas)
                let emailExists = false;
                empresas.forEach(empresa =>  {
                    console.log("comparando ", empresa.empresa, empresa.email)
                    if (email === empresa.email) {
                        //TODO return in specific languages and check where this message is returning, must be under the box. 
                        emailExists = true;
                        console.log("Mail existe")
                    }
                });
                this.setState({ emailExists });
                this.props.setEmailExists(emailExists);
            })
    }

    

    render() {
        let message = (this.state.emailExists) ? TEXTOS.semail[this.props.idioma] : "";
        return (<>
            <Input
                type="email"
                name="email"
                id="exampleEmail"
                value={this.props.email}
                onChange={this.props.onChange}
                defaultValue={this.state.value} onBlur={this.handleBlur}
            />
            <p className="alerta">{message}</p>
        </>);
    }
}


export default Email;
//onBlur is created everytime the component loses the focus (in this case is not being clicked on anymore, so when the person leaves the email box it will check if it is valid)