import React from 'react';
import emailjs from 'emailjs-com';
import { TEXTOS } from './textos';
import { Redirect } from "react-router-dom";
import { Button } from 'reactstrap';
//import './ContactUs.css';

export default class ContactUs extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user_name: "",
      user_email: "",
      message: "",
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.volver = this.volver.bind(this);

  };

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  sendEmail(e) {
    e.preventDefault();

    emailjs.sendForm('gmail', 'contactus', e.target, 'user_751QASu6V5ooSGjH11Szn')
      .then((result) => {
        console.log(result.text);
      }, (error) => {
        console.log(error.text);
      });
  }

  volver() {
    this.setState({ volver: true });
  }

  render() {

    if (this.state.volver === true) {
      return <Redirect to="/App" />;
    }

    return (
      <form className="contact-form" onSubmit={this.sendEmail}>
        <label>{TEXTOS.sname[this.props.idioma]}</label>
        <input value={this.state.user_name} onChange={this.handleInputChange} type="text" name="user_name" />
        <label>Email </label>
        <input value={this.state.user_email} onChange={this.handleInputChange} type="email" name="user_email" />
        <label>{TEXTOS.scomment[this.props.idioma]}</label>
        <textarea value={this.state.message} onChange={this.handleInputChange} name="message" />
        <Button variant="primary" type="submit">{TEXTOS.ssubmit[this.props.idioma]}</Button>
        <Button onClick={this.volver}>{TEXTOS.sreturn[this.props.idioma]}</Button>
      </form>
    );
  }
}

//<form id="myform" onsubmit="emailjs.sendForm('mailjet', 'city_request', this).then(reset()); return false;" method="post"></form>

//You must install this before anything else: npm install emailjs-com --save
//https://www.emailjs.com/docs/tutorial/adding-email-service/  
//user ID está em account
//template criamos no inicio
//na pagina do email tem que colocar os campos Name:{{user_name}}; Email:{{user_email}}; Message:{{message}}
//<input type="submit" value="Send" />