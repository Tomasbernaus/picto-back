import React from 'react';
import { API_URL, IMG_URL } from "./database.js";
import {TEXTOS} from './textos';

class Drop extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            tea: [],
            id: 1,
            tipoActual:"tipo_CA"
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.compare = this.compare.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    componentDidMount() {

        fetch(API_URL + "tea_tea?_p=0&_size=100", { method: "GET" })
            .then(data => data.json())
            .then(dadesConvertides => {
                //console.log(dadesConvertides);
                this.setState({ tea: [...this.state.tea, ...dadesConvertides] });
            })
            .catch(err => console.log(err));

        // , teas: teas, id: teas[0].id 
        fetch(API_URL + "tea_tea?_p=1&_size=100", { method: "GET" })
            .then(data => data.json())
            .then(dadesConvertides => {
                //console.log(dadesConvertides);
                this.setState({ tea: [...this.state.tea, ...dadesConvertides] });
            })
            .catch(err => console.log(err));
    }

    compare( a, b ) {
        let tipo = "tipo_CA";
        if (this.props.idioma === 1) tipo = "tipo_ES";
        if (this.props.idioma === 2) tipo = "tipo_EN";

        if ( a[tipo] < b[tipo] ){
          return -1;
        }
        if ( a[tipo] > b[tipo] ){
          return 1;
        }
        return 0;
      }


    render() {

        if (this.state.tea.length === 0) {
            return <h1>{TEXTOS.sloading[this.props.idioma]}</h1>
        }

         let tipoIdioma = "tipo_CA";
        if (this.props.idioma===1) tipoIdioma="tipo_ES";
        if (this.props.idioma===2) tipoIdioma="tipo_EN";

        let options = this.state.tea.sort(this.compare).map(el => <option key={el.id} value={el.id}>{el[tipoIdioma]}</option>);
        let actual = this.state.tea.filter(el => el.id == this.state.id)[0];

    
        return (
            <>
                <select onChange={this.handleInputChange} name="id">
                    {options}
                </select>
                <br />

                <img src={IMG_URL + actual.pictograma} width="100px" />
          
            </>
        );
    }
}


export default Drop;