import './Form.css';
import React from 'react';
import { Col, Button } from 'reactstrap';
import { Form } from 'react-bootstrap';
import Email from './Email.js';
import Drop from './Dropdown.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import { TEXTOS } from './textos';
import { API_URL, IMG_URL } from "./database.js";
import { Redirect } from "react-router-dom";


export default class Formulario extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      empresa: "",
      address: "",
      contato: "",
      contrasena: "",
      email: "",
      number: "",
      zip:"",
      city:"",
      provincia:"",
      volver: false,
      emailExists: true,
    };


    this.handleInputChange = this.handleInputChange.bind(this);
    this.creaEmpresas = this.creaEmpresas.bind(this);
    this.setEmailExists = this.setEmailExists.bind(this);
    this.volver = this.volver.bind(this);
  };

  setEmailExists(value){
    this.setState({emailExists: value});
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }


  creaEmpresas(e) {
    e.preventDefault();

    let prod = {
      empresa: this.state.empresa,
      address: this.state.address,
      email: this.state.email,
      number: this.state.number,
      contrasena: this.state.contrasena,
      zip:this.state.zip,
      city:this.state.city,
      provincia:this.state.provincia

    }
    console.log(prod)
    fetch(API_URL + "tea_empresas", {
      method: 'POST',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify(prod)
    })
      .then(data => data.json())
      .then(data => console.log(data))
      .then(() => this.setState({ volver: true }))
      .catch(err => console.log(err));

  }

  volver() {
    this.setState({ volver: true });
  }

  render() {

    if (this.state.volver===true){
       return <Redirect to="/App" />;
    }
    return (
      <>
        <Form onSubmit={this.creaEmpresas}>
          <h1 className="Title">{TEXTOS.sform[this.props.idioma]}</h1>

          <Form.Row>

            <Form.Group controlId="formGridName">
              <Form.Label>{TEXTOS.scompany[this.props.idioma]}</Form.Label>
              <Form.Control onChange={this.handleInputChange} name="empresa" value={this.state.empresa} />
            </Form.Group>

            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Label>Email</Form.Label>
              <Email setEmailExists={this.setEmailExists} onChange={this.handleInputChange} name="email" idioma={this.props.idioma} email={this.state.email}/>
            </Form.Group>

            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label>{TEXTOS.spassword[this.props.idioma]}</Form.Label>
              <Form.Control onChange={this.handleInputChange} name="contrasena" type="password"  value={this.state.contrasena} />
            </Form.Group>
          </Form.Row>

          <Form.Group controlId="formGridAddress1">
            <Form.Label>{TEXTOS.saddress[this.props.idioma]}</Form.Label>
            <Form.Control onChange={this.handleInputChange} name="address" value={this.state.address}  />
          </Form.Group>

          <Form.Group controlId="formGridNumber">
            <Form.Label>{TEXTOS.sphone[this.props.idioma]}</Form.Label>
            <Form.Control onChange={this.handleInputChange} name="number" value={this.state.number} />
          </Form.Group>

          <Form.Row>
            <Form.Group as={Col} controlId="formGridCity">
              <Form.Label>{TEXTOS.scity[this.props.idioma]}</Form.Label>
              <Form.Control onChange={this.handleInputChange} name="city" value={this.state.city} />
            </Form.Group>

            <Form.Group as={Col} controlId="formGridState">
              <Form.Label>{TEXTOS.sstate[this.props.idioma]}</Form.Label>
              <Form.Control onChange={this.handleInputChange} name="provincia" value={this.state.provincia} />
            </Form.Group>

            <Form.Group as={Col} controlId="formGridZip">
              <Form.Label>{TEXTOS.szip[this.props.idioma]}</Form.Label>
              <Form.Control onChange={this.handleInputChange} name="zip" value={this.state.zip} />
            </Form.Group>
          </Form.Row>

          <Form.Group as={Col} controlId="dropdown-basic">
            <Form.Label>{TEXTOS.sselect[this.props.idioma]}</Form.Label>
            <Drop idioma={this.props.idioma} />
          </Form.Group>

          <Button disabled={this.state.emailExists} variant="primary" type="submit">{TEXTOS.ssubmit[this.props.idioma]}</Button>
          <Button onClick={this.volver}>{TEXTOS.sreturn[this.props.idioma]}</Button>

        </Form>
      </>
    );

  }
}


