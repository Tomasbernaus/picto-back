import React from 'react';
import { Button } from 'reactstrap';
import { Form } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Form.css';
import { TEXTOS } from './textos';
import { Redirect } from "react-router-dom";


export default class Contato extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            volver: false,
        };
        this.volver = this.volver.bind(this);
    };

    volver() {
        this.setState({ volver: true });
    }

    render() {

        if (this.state.volver === true) {
            return <Redirect to="/App" />;
        }

        return (<>
            <Form>
                <h1 className="Title">{TEXTOS.scontact[this.props.idioma]}</h1>

                <Form.Row>
                    <Form.Group controlId="formGridName">
                        <Form.Label><p>{TEXTOS.sinfo[this.props.idioma]}</p></Form.Label>
                    </Form.Group>
                </Form.Row>

                <Form.Row>
                    <Form.Group controlId="formGridName">
                        <Form.Label>Email</Form.Label>
                        <Form.Control placeholder="Insert email here" />
                    </Form.Group>
                </Form.Row>

                <Form.Row>
                    <Form.Group controlId="formGridName">
                        <Form.Label>{TEXTOS.sname[this.props.idioma]}</Form.Label>
                        <Form.Control placeholder="Insert name here" />
                    </Form.Group>
                </Form.Row>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>{TEXTOS.scomment[this.props.idioma]}</Form.Label>
                    <Form.Control as="textarea" rows="3" />
                </Form.Group>
                <br />
                <Button variant="primary" type="submit">{TEXTOS.ssubmit[this.props.idioma]}</Button>
                <Button onClick={this.volver}>{TEXTOS.sreturn[this.props.idioma]}</Button>
            </Form>
        </>
        );
    }
}
